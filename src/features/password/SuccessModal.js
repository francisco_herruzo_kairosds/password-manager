import { useTranslation } from 'react-i18next'
import { Modal } from '../../app/components'

const SuccessModal = () => {
  const { t } = useTranslation()

  return (
    <>
      <Modal type="success" buttonText={t('modal.success.button')}>
        <h2>{t('modal.success.title')}</h2>
        <div>{t('modal.success.text')}</div>
      </Modal>
    </>
  )
}

export default SuccessModal
