import { render, fireEvent } from '@testing-library/react'
import Wizard from '../Wizard'

describe.skip('Wizard', () => {
  it('should hide the wizard if you click in the cancel button', () => {
    const { getByText, queryByText } = render(
      <Wizard>
        <div>Step 1</div>
        <div>Step 2</div>
      </Wizard>
    )
    const cancelButton = getByText('Cancelar')

    fireEvent.click(cancelButton)

    expect(queryByText('Step 2')).not.toBeInTheDocument()
  })

  it('renders the wizard in the step 1', () => {
    const { getByText, queryByText } = render(
      <Wizard>
        <div>Step 1</div>
        <div>Step 2</div>
      </Wizard>
    )

    expect(getByText('Step 1')).toBeInTheDocument()
    expect(queryByText('Step 2')).not.toBeInTheDocument()
  })

  it('shows the step 2 when you click in next button', () => {
    const { getByText, queryByText } = render(
      <Wizard>
        <div>Step 1</div>
        <div>Step 2</div>
      </Wizard>
    )
    const nextButton = getByText('Siguiente')

    fireEvent.click(nextButton)

    expect(queryByText('Step 1')).not.toBeInTheDocument()
    expect(getByText('Step 2')).toBeInTheDocument()
  })

  it('hide the next button in the last step', () => {
    const { getByText, queryByText } = render(
      <Wizard>
        <div>Step 1</div>
        <div>Step 2</div>
        <div>Step 2</div>
      </Wizard>
    )
    const nextButton = getByText('Siguiente')

    fireEvent.click(nextButton)
    fireEvent.click(nextButton)

    expect(queryByText('Crea tu Password Manager')).not.toBeInTheDocument()
  })
  it('shows the send button in the last step', () => {
    const { getByText } = render(
      <Wizard>
        <div>Step 1</div>
        <div>Step 2</div>
        <div>Step 2</div>
      </Wizard>
    )
    const nextButton = getByText('Siguiente')

    fireEvent.click(nextButton)
    fireEvent.click(nextButton)

    expect(getByText('Crear')).toBeInTheDocument()
  })
})
