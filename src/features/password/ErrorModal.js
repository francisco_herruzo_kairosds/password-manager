import { useTranslation } from 'react-i18next'
import { Modal } from '../../app/components'

const ErrorModal = () => {
  const { t } = useTranslation()

  return (
    <>
      <Modal type="error" buttonText={t('modal.error.button')}>
        <h2>{t('modal.error.title')}</h2>
        <div>{t('modal.error.text')}</div>
      </Modal>
    </>
  )
}

export default ErrorModal
