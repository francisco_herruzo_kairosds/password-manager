import { Children } from 'react'
import { WizardHeader } from './WizardHeader'
import './Wizard.scss'

const Wizard = ({ children, currentPage }) => {
  const pages = Children.toArray(children)
  const getCurrentPage = pages[currentPage]

  return (
    <div className="wizard">
      <WizardHeader pages={pages} currentPage={currentPage} />
      <WizardPages currentPage={getCurrentPage}>{children}</WizardPages>
    </div>
  )
}

const WizardPages = ({ currentPage }) => {
  return <div>{currentPage}</div>
}

export default Wizard
