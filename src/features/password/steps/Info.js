import { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Button, ButtonType } from '../../../app/components'
import Head from '../../../app/img/group.svg'
import Safe from '../../../app/img/group-3.svg'

const Info = ({ toggleWizard, goNextPage }) => {
  const { t } = useTranslation()
  const [checked, setChecked] = useState(false)
  const isDisabledNextButton = !checked

  const handleCheckboxChange = () => setChecked((current) => !current)

  return (
    <>
      <div className="wizard__step">
        <div className="step">
          <div className="step__title">
            <h2 className="step__title-text">{t('title')}</h2>
            <div className="step__title-border" />
          </div>
          <div className="step__column-wrapper">
            <div className="step__column">
              <div className="step__column-img">
                <img src={Head} alt="Head" />
              </div>
              <div className="step__column-text">{t('info.column1.text')}</div>
            </div>
            <div className="step__column">
              <div className="step__column-img">
                <img src={Safe} alt="Caja fuerte" />
              </div>
              <div className="step__column-text">{t('info.column2.text')}</div>
            </div>
          </div>
          <div className="step__paragraph">
            <div className="step__paragraph-title">
              {t('info.paragraph1.title')}
            </div>
            <div className="step__paragraph-text">
              {t('info.paragraph1.text')}
            </div>
          </div>
          <div className="step__paragraph">
            <div className="step__paragraph-title">
              {t('info.paragraph2.title')}
            </div>
            <div className="step__paragraph-text">
              {t('info.paragraph2.text')}
            </div>
          </div>
          <div className="step__check">
            <label>
              <input
                type="checkbox"
                checked={checked}
                onChange={handleCheckboxChange}
              />
              {t('info.checkbox')}
            </label>
          </div>
        </div>
      </div>
      <div className="wizard__footer">
        <Button onClick={toggleWizard} type={ButtonType['link-secondary']}>
          {t('button.cancel')}
        </Button>
        <Button
          onClick={goNextPage}
          disabled={isDisabledNextButton}
          type={ButtonType.default}
        >
          {t('button.next')}
        </Button>
      </div>
    </>
  )
}

export default Info
