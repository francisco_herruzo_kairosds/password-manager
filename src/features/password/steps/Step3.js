import { useTranslation } from 'react-i18next'
import { Button, ButtonType } from '../../../app/components'

const Step3 = ({ toggleWizard, handleSubmitForm }) => {
  const { t } = useTranslation()

  return (
    <>
      <div className="wizard__step">
        <div className="step">
          <div className="step__title">
            <h2 className="step__title-text">{t('title')}</h2>
            <div className="step__title-border" />
          </div>
          <div className="step__paragraph">
            <div className="step__paragraph-title">{t('step3.title')}</div>
            <div className="step__paragraph-text">{t('step3.lorem')}</div>
          </div>
        </div>
      </div>
      <div className="wizard__footer">
        <Button onClick={toggleWizard} type={ButtonType['link-secondary']}>
          {t('button.cancel')}
        </Button>
        <Button onClick={handleSubmitForm} type={ButtonType.primary}>
          {t('button.send')}
        </Button>
      </div>
    </>
  )
}

export default Step3
