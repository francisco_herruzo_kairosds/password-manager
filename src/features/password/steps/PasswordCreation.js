import { useState, useEffect, useRef } from 'react'
import { nanoid } from 'nanoid'
import { useTranslation } from 'react-i18next'
import { Input, InputPassword } from '../../../app/components/'
import { Button, ButtonType } from '../../../app/components'

const PasswordCreation = ({
  form,
  handleInputChange,
  toggleWizard,
  goNextPage,
}) => {
  const { t } = useTranslation()
  const firstRender = useRef(true)
  const [errors, setErrors] = useState([])

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false
      return
    }

    const formValidation = () => {
      const { password, repeatPassword } = form
      const currentErrors = []

      if (password.length < 8) {
        currentErrors.push([t('input.error.short')])
      }
      if (password === password.toLowerCase()) {
        currentErrors.push([t('input.error.uppercase')])
      }
      if (!/\d+/g.test(password)) {
        currentErrors.push([t('input.error.number')])
      }
      if (password !== repeatPassword) {
        currentErrors.push([t('input.error.notMatch')])
      }

      setErrors(currentErrors)
    }

    formValidation()
  }, [form, t])

  const isDisabledNextButton = errors.length

  const calculatePasswordStrengthLevel = () => {
    if (errors.length) return 1
    if (!errors.length && form.password.length > 12) return 3
    if (!errors.length && form.password) return 2
  }

  return (
    <>
      <div className="wizard__step">
        <div className="step">
          <div className="step__title">
            <h2 className="step__title-text">{t('title')}</h2>
            <div className="step__title-border" />
          </div>
          <div className="step__paragraph-text">{t('password.paragraph1')}</div>
          <div className="step__form-password">
            <InputPassword
              label={t('input.password.label')}
              name="password"
              placeholder={t('input.password.placeholder')}
              onChange={handleInputChange}
              strength={calculatePasswordStrengthLevel()}
            />
            <InputPassword
              label={t('input.repeatPassword.label')}
              name="repeatPassword"
              placeholder={t('input.repeatPassword.placeholder')}
              onChange={handleInputChange}
            />
          </div>
          <div className="step__form-errors">
            <ul>
              {errors &&
                errors.map((error) => {
                  return <li key={nanoid()}>{error}</li>
                })}
            </ul>
          </div>
          <div className="step__form-hint">
            <div className="step__paragraph-text">
              {t('password.paragraph2')}
            </div>
            <Input
              label={t('input.hint.label')}
              name="hint"
              placeholder={t('input.hint.placeholder')}
              value=""
              onChange={handleInputChange}
            />
          </div>
        </div>
      </div>
      <div className="wizard__footer">
        <Button onClick={toggleWizard} type={ButtonType['link-secondary']}>
          {t('button.cancel')}
        </Button>
        <Button
          onClick={goNextPage}
          disabled={isDisabledNextButton}
          type={ButtonType.default}
        >
          {t('button.next')}
        </Button>
      </div>
    </>
  )
}

export default PasswordCreation
