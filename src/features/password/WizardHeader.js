import { nanoid } from 'nanoid'
import { Check } from 'react-feather'

export const WizardHeader = ({ pages, currentPage }) => {
  const isActivePage = (index) => {
    if (currentPage === index) return 'wizard__header-step--active'
  }
  const isPageDone = (index) => {
    if (index < currentPage) return 'wizard__header-step--done'
  }

  return (
    <div className="wizard__header">
      {pages.map((page, index) => {
        return (
          <div
            className={`wizard__header-step ${isActivePage(index)} ${isPageDone(
              index
            )}`}
            key={nanoid()}
          >
            {index < currentPage ? <Check size={20} /> : index + 1}
          </div>
        )
      })}
    </div>
  )
}
