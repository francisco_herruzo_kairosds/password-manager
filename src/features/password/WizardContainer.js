import { useState } from 'react'
import { submitForm } from '../../services/api'
import Wizard from './Wizard'
import SuccessModal from './SuccessModal'
import ErrorModal from './ErrorModal'
import { Info, PasswordCreation, Step3 } from './steps'

const WizardContainer = () => {
  const [showWizard, setShowWizard] = useState(true)
  const [showSuccessModal, setShowSuccessModal] = useState(false)
  const [showErrorModal, setshowErrorModal] = useState(false)
  const [currentPage, setCurrentPage] = useState(0)
  const [form, setForm] = useState({
    password: '',
    repeatPassword: '',
    hint: '',
  })

  const handleInputChange = (event) => {
    const { name, value } = event.target
    setForm({ ...form, [name]: value })
  }
  const goNextPage = () => setCurrentPage((index) => index + 1)
  const toggleWizard = () => setShowWizard((current) => !current)
  const handleSubmitForm = async () => {
    const { password, repeatPassword, hint } = form

    try {
      const { status } = await submitForm(password, repeatPassword, hint)
      status === 200 ? setShowSuccessModal(true) : setshowErrorModal(true)
      setShowWizard((current) => !current)
    } catch (error) {
      setShowWizard((current) => !current)
      setshowErrorModal(true)
    }
  }
  return (
    <>
      {showWizard && (
        <Wizard currentPage={currentPage}>
          <Info toggleWizard={toggleWizard} goNextPage={goNextPage} />
          <PasswordCreation
            form={form}
            handleInputChange={handleInputChange}
            toggleWizard={toggleWizard}
            goNextPage={goNextPage}
          />
          <Step3
            toggleWizard={toggleWizard}
            handleSubmitForm={handleSubmitForm}
          />
        </Wizard>
      )}
      {showSuccessModal && <SuccessModal />}
      {showErrorModal && <ErrorModal />}
    </>
  )
}

export default WizardContainer
