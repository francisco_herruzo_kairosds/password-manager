import { useState } from 'react'
import { Eye, EyeOff } from 'react-feather'
import './Input.scss'

export const Input = ({ label, placeholder, type, name, onChange, icon }) => {
  return (
    <div>
      <label className="input--label">{label}</label>
      <div className="input--wrapper">
        <input
          className="input"
          placeholder={placeholder}
          name={name}
          type={type}
          onChange={onChange}
        />
        {icon}
      </div>
    </div>
  )
}

export const InputPassword = ({
  label,
  placeholder,
  name,
  onChange,
  strength,
}) => {
  const [passwordShown, setPasswordShown] = useState(false)
  const handleClick = () => setPasswordShown((current) => !current)

  return (
    <div className="test">
      <Input
        type={passwordShown ? 'text' : 'password'}
        label={label}
        name={name}
        placeholder={placeholder}
        onChange={onChange}
        icon={
          passwordShown ? (
            <EyeOff size={20} onClick={handleClick} />
          ) : (
            <Eye size={20} onClick={handleClick} />
          )
        }
      />
      {strength && <PasswordStrength level={strength} />}
    </div>
  )
}

const PasswordStrength = ({ level }) => (
  <div
    className={`input__password-strength input__password-strength-${level}`}
  ></div>
)
