import { render } from '@testing-library/react'
import { InputPassword } from './Input'

describe('Input', () => {
  it('renders Password Manager title', () => {
    const { getByPlaceholderText } = render(
      <InputPassword
        label="Crea tu Contraseña Maestra"
        placeholder="Contraseña"
        value=""
        handleChange={() => {}}
      />
    )

    const passwordInput = getByPlaceholderText('Contraseña')

    expect(passwordInput).toBeInTheDocument()
  })
})
