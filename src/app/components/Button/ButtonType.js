const ButtonType = {
  default: 'default',
  primary: 'primary',
  'link-primary': 'link-primary',
  'link-secondary': 'link-secondary',
}

export default ButtonType
