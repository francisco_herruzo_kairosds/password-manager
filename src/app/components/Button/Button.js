import './Button.scss'
import ButtonType from './ButtonType'

const Button = ({ type, onClick, disabled, children }) => {
  const buttonType = ButtonType[type] || type.default

  return (
    <>
      <button
        type="button"
        disabled={disabled}
        onClick={onClick}
        className={`btn btn--${buttonType}`}
      >
        {children}
      </button>
    </>
  )
}

export default Button
