import { Check, AlertTriangle } from 'react-feather'
import { Button, ButtonType } from '../'
import './Modal.scss'

const Modal = ({ type = 'success', buttonText, children }) => {
  return (
    <div className="modal">
      <div className="modal__content-wrapper">
        {type === 'success' ? <Check size={72} /> : <AlertTriangle size={72} />}
        <div className="modal__content">{children}</div>
      </div>
      <div className="modal__footer">
        <Button onClick={() => {}} type={ButtonType['link-primary']}>
          {buttonText}
        </Button>
      </div>
    </div>
  )
}

export default Modal
