import { PasswordWizard } from '../features/password'
import './styles/App.scss'

function App() {
  return (
    <div className="App">
      <PasswordWizard />
    </div>
  )
}

export default App
