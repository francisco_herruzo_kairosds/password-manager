import { render } from '@testing-library/react'
import App from './App'

describe('App', () => {
  it.skip('renders Password Manager title', () => {
    const { getByText } = render(<App />)

    const wizardTitle = getByText(/Crea tu Password Manager/i)

    expect(wizardTitle).toBeInTheDocument()
  })
})
